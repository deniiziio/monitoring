import sys, socket, psutil, datetime, yagmail, platform, os, argparse, keyring
from time import *


def cmdline_args():                                                                                                     #Funktion ArgumentParser

    parser = argparse.ArgumentParser(description='Das ist eine Hilfe, unten gibt es weitere '                           #Hilfe beschreibung
                                                 'optionale Argumente um das Programm zu bedienen')
    parser.add_argument("-i", "--info", action='store_true', help="Autoren dieses Programmes")                          #Argument um die Autoren dieses Programms anzuzeigen
    parser.add_argument("-e", "--easter", action='store_true', help="Easteregg")

    args = parser.parse_args()
    if args.info:                                                                                                       #ausgabe der Autoren
        print("Deniz, Kenneth und JD")
        sys.exit()
    if args.easter:                                                                                                     #nocomment
        print("right click me and open link: https://www.youtube.com/watch?v=dQw4w9WgXcQ")
        sys.exit()


def console():                                                                                                          #Gibt in der Konsole wichtige Informationen aus

    hostname = socket.gethostname()                                                                                     #Ausgabe Hostname
    ip_address = socket.gethostbyname(hostname)                                                                         #liest die IP-Adresse aus
    now = datetime.datetime.now()                                                                                       #Datum und Zeit "In diesem Moment"
    UsedDisk = psutil.disk_usage("..").used / 1000000                                                                   #Verwendeter Speicherplatz umgerechnet in MB
    CPU_Clock = psutil.cpu_freq().current                                                                               #Aktuelle CPU Geschwindigkeit
    Used_in_perc = psutil.disk_usage("..").percent                                                                      #Speichernutzung in %
    Uhrzeit = now.strftime('%H:%M:%S')                                                                                  #Uhrzeit in Stunden/Minuten/Sekunden
    Datum = now.strftime('%d/%m/%Y')                                                                                    #Datum in Tagen/Monaten/Jahren

    print("Dieses Programm ist ein Tool zum Monitoren")                                                                 #Infotext
    print(Datum)                                                                                                        #Konsolen ausgabe mit Daten über den Computer
    print(Uhrzeit)                                                                                                      #
    print("\n")                                                                                                         #
    print(f"Herzlich Willkommen:    {os.getlogin()}")                                                                   #
    print(f"Dateipfad:              {os.getcwd()}")                                                                     #
    print(f"Python Version:         {platform.python_version()}")                                                       #
    print(f"Betriebssystem:         {platform.system()}")                                                               #
    print(f"prozessor:              {platform.processor()}")                                                            #
    print(f"Hostname: 		        {hostname}")                                                                        #
    print(f"IP Address: 	    	{ip_address}")                                                                      #
    print(f"CPU Clock:              {CPU_Clock} MHZ")                                                                   #
    print(f"Diskusage:              {UsedDisk} MB")                                                                     #
    print(f"Used in %:              {Used_in_perc} %")                                                                  #


def send_email(Text):                                                                                                   #Stellt die Verbindung zum Email Server her und verschickt eine Mail, bei abruf

    password = keyring.get_password("Monitoring", "deniiziio@gmail.com")
    yag = yagmail.SMTP(user='deniiziio@gmail.com', password= password)                                                  #Anmelde Informationen
    content = (Text)                                                                                                    #Textinhalt der Mail
    yag.send(to='deniiziio@gmail.com', subject='Log Datei',                                                             #erstellt die Email Struktur
             contents = content, attachments='/home/deniz/PycharmProjects/Monitoring/venv/logfile.txt')


def log_writing():                                                                                                      #Schreibt die Log Datei

    hostname = socket.gethostname()                                                                                     #Ausgabe Hostname
    now = datetime.datetime.now()                                                                                      #Datum und Zeit "In diesem Moment"
    UsedDisk = psutil.disk_usage("..").used / 1000000000                                                                #Verwendeter Speicherplatz umgerechnet in GB
    UsedDisk_short = round(UsedDisk, 2)                                                                                 #Rundet verwendeten Speicherplatz auf 2 Zahlen nach dem Kommar
    UsedDisk_str = str(UsedDisk_short)                                                                                  #Formt den Float UsedDisk_short in ein String
    Used_in_perc = psutil.disk_usage("..").percent                                                                      #Speichernutzung in %
    Used_in_perc_str = str(Used_in_perc)                                                                                #Formt den Float Used_in_perc in ein String
    uhrzeit = now.strftime('%H:%M:%S')                                                                                  #Uhrzeit in Stunden/Minuten/Sekunden
    datum = now.strftime('%d/%m/%Y')                                                                                    #Datum in Tagen/Monaten/Jahren
    process = psutil.Process()                                                                                          #Prozessabfrage
    process_number = process.io_counters().read_count                                                                   #Prozesse werden zusammengezählt und ausgegeben
    process_number_str = str(process_number)                                                                            #Formt den Float process_number in ein String

    if Used_in_perc < 70:                                                                                               #Zustand des Speichers
        warning_sign = 'Normal'                                                                                         #
    if (Used_in_perc >= 70) and (Used_in_perc <= 90):                                                                   #
        warning_sign = 'Attention'                                                                                      #
    if Used_in_perc > 90:                                                                                               #
        warning_sign = 'Critical'                                                                                       #


    try:                                                                                                                #schreibt die log Datei
        file = open("/home/deniz/PycharmProjects/Monitoring/venv/logfile.txt", 'a+')                                      #log.txt muss vorher erstellt werden
    except Exception:                                                                                                   #
        print("Datei kann nicht geöffnet werden")                                                                       #
        sys.exit(0)                                                                                                     #
    file.write(datum)
    file.write("\t")
    file.write(uhrzeit)
    file.write("\t")
    file.write(warning_sign)
    file.write("\t")
    file.write("User:")
    file.write(hostname)
    file.write("\t\t")
    file.write("Diskusage:")
    file.write("\t")
    file.write(UsedDisk_str)
    file.write("GB")
    file.write("\t")
    file.write(Used_in_perc_str)                                                                                        #
    file.write("%")                                                                                                     #
    file.write("\t")                                                                                                    #
    file.write("Processes:")                                                                                            #
    file.write("\t")                                                                                                    #
    file.write(process_number_str)                                                                                      #
    file.write("\n")                                                                                                    #
    file.close()                                                                                                        #


def sending_email_strg_prcs(process_limit_int):                                                                         #Verfasst die Email die verschickt werden soll mit 2 Parametern, Speicher und Prozesse

    process = psutil.Process()                                                                                          #Prozessabfrage
    process_number = process.io_counters().read_count                                                                   #Prozesse werden zusammengezählt und ausgegeben
    Used_in_perc = psutil.disk_usage("..").percent                                                                      #Speichernutzung in %
    now = datetime.datetime.now()                                                                                       #Datum und Zeit "In diesem Moment"
    Uhrzeit = now.strftime('%H:%M:%S')                                                                                  #Uhrzeit in Stunden/Minuten/Sekunden

    if (Used_in_perc >= 70) and (Used_in_perc <= 90):                                                                   #Email wenn Speicher über 70% voll ist
        send_email("Speicher Auslastung liegt zwischen 70% und 90% Prozent")
        print("Email sent successfully!     storag:70 - 90      "+ Uhrzeit)

    elif Used_in_perc > 90:                                                                                             #Email wenn Speicher über 90% voll ist
        send_email("Speicher Auslastung liegt über 90% Prozent!!!!")
        print("Email sent successfully!     storag:>90          "+ Uhrzeit)

    elif Used_in_perc < 70 and process_number > process_limit_int :                                                     #Email bei normalem Speicher, und überschreitung des Prozess limits
        send_email("Die Speicherauslastung ist normal." +"\n"+
                   "Die Anzahl der Prozesse übersteigt " + process_limit)
        print("Email sent successfully!     storag:<70          "+ Uhrzeit)

    else:                                                                                                               #Konsolen Ausgabe, dass keine Mail verschickt wurde
        print("No Email was sent        "+ Uhrzeit)


def main_loop():
    while True:
        log_writing()                                                                                                   #schreibt log Datei
        sending_email_strg_prcs(process_limit_int)                                                                      #Sendet Email
        sleep(10)                                                                                                       #wartet 10 sek


def process_query():                                                                                                    #Funktion für die Abfrage zur Anzahl der zu kontrollierenden Prozesse

    global process_limit_int                                                                                            #macht die Variable process_limit int global
    global process_limit                                                                                                #macht die Variable process_limit global

    print("Ab wieviel prozessen soll eine Mail verschickt werden?")                                                     #Abfrage zur Anzahl der zu kontrollierenden Prozesse
    process_limit = input()                                                                                             #input von der Tastatur
    process_limit_int = int(process_limit)                                                                              #Formt den String process_limit in ein integer


cmdline_args()
console()                                                                                                               #ruft die Funktion console() auf
process_query()                                                                                                         #ruft die Funktion process_query() auf
main_loop()                                                                                                             #ruft die Funktion main_loop() auf

